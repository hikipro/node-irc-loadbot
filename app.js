/*
	node-irc-loadbot
	Written by Hiki Hwang ( hikipro95 [at] gmail )

	Original concept by pjm0616 ( http://sigkill.kr/files/scripts/irc_loadavg_bot.pl )
 */

console.log(
    "node-irc-loadbot\n" +
    "Written by Hiki Hwang\n"
);

const os = require('os');
const http = require('http');
const exec = require('child_process').exec;

const stdin = process.openStdin();

const isWindows = (os.platform() == "win32" ? true : false);

// -- config start. edit this object.
const config = {
    use_ssl: true,
    hostname: os.hostname(),
    nick: "로드봇^test",
    hostname_hide: true,
    nickserv_pass: "",
    autojoin: ["#ozinger"],
    irc_server_addr: "ssl.ozinger.org",
    irc_server_port: 16666
};
// -- config end.

var commands = [];

var client = null;

function init() {
	addCommands();

	stdin.setRawMode(true);
	stdin.resume();

	stdin.setEncoding('utf8');

	stdin.on('data', keyProc);

	process.on('uncaughtException', (err) => {
		log("Uncaught exception occurred.");
		console.log(err);
	});

	return true;
}

function proc() {
	client = require((config.use_ssl ? 'tls' : 'net')).connect({
		host: config.irc_server_addr,
		port: config.irc_server_port,
		rejectUnauthorized: false
	}, () => {
		log("[+] connected at " + config.irc_server_addr);

		send("USER loadbot 8 * :node-loadbot (" + config.hostname + ")");
		send("NICK " + config.nick);
	});

	client.setEncoding('utf8');

	client.on('data', (data) => {
		var lines = data.toString().split('\r\n');
		for (var idx in lines) {
			if (!lines[idx]) { return; }

			var match;
			if (match = lines[idx].match(/^PING :(.+)/)) {
				send("PONG :" + match[1]);
			} else if (match = lines[idx].match(/^:([^\s]+?) 001/)) {
				if (config.hostname_hide) {
					send("MODE " + config.nick + " +x");
				}

				if (config.nickserv_pass) {
					send("NICKSERV IDENTIFY " + config.nick + " " + config.nickserv_pass);
				}

				for (var idx in config.autojoin) {
					send("JOIN " + config.autojoin[idx]);
				}
			} else if (match = lines[idx].match(/^:([^\s]+) 433/)) {
				log("[!] nickname is already using.");
				send("NICK " + (config.nick += "_"));
			} else if (match = lines[idx].match(/^\:([^\s]+)\!([^\s]+)@([^\s]+) PRIVMSG ([^\s:]+) :(.+)/)) {
				for (var idx in commands) {
					var command = commands[idx];

					for (var idx2 in command[0]) {
						var cmd = command[0][idx2];

						if (match[5].toLowerCase().indexOf(cmd) == 0) {
							command[1](match[4], match[5]);
						}
					}
				}
			} else if (match = lines[idx].match(/^\:([^\s]+)\!([^\s]+)@([^\s]+) JOIN :\#(.*)/)) {
				if (match[1] == config.nick) {
					log("[-] joined at #" + match[4]);
				}
			} else if (match = lines[idx].match(/^\:([^\s]+)\!([^\s]+)@([^\s]+) PART \#([^\s]*?) :(.*)/)) {
				if (match[1] == config.nick) {
					log("left at #" + match[4] + " (" + match[5] + ")");
				}
			} else if (match = lines[idx].match(/^\:([^\s]+)\!([^\s]+)@([^\s]+) INVITE ([^\s]+?) :#(.*)/)) {
				if (match[4] == config.nick) {
					log("invited by " + match[1] + " at #" + match[5]);
					send("JOIN #" + match[5]);
				}
			}
		}
	});

	return true;
}

function addCommands() {
	commands.push([
		[config.nick, "!info"],
		(chan, msg) => {
			send("PRIVMSG " + chan + " :" + os.hostname() + " (" + os.platform() + "_" + os.arch() + "_" + os.release() + ", uptime " + getUptimeStr() + (os.platform() != "win32" ? ", loadavg " + loadavg_str : "") + ", memory " + getFreeMemory() + " / " + getTotalMemory() + ")");
		}
	], [
		["!로드", "!loadavg"],
		(chan, msg) => {
			if (isWindows) {
				send("PRIVMSG " + chan + " :" + config.hostname);
			} else {
				exec("uptime", (error, stdout, stderr) => {
					send("PRIVMSG " + chan + " :" + config.hostname + " | " + stdout);
				});
			}
		}
	], [
		["!uptime", "!업타임"],
		(chan, msg) => {
			send("PRIVMSG " + chan + " :" + getUptimeStr());
		}
	], [
		["!커널", "!kernel"],
		(chan, msg) => {
			if (isWindows) { return; }

			exec("uname -sr", (error, stdout, stderr) => {
				send("PRIVMSG " + chan + " :" + config.hostname + " | " + stdout);
			});
		}
	], [
		["!uname"],
		(chan, msg) => {
			if (isWindows) { return; }

			exec("uname -a", (error, stdout, stderr) => {
				send("PRIVMSG " + chan + " :" + stdout);
			});
		}
	], [
		["!eip"],
		(chan, msg) => {
			http.get("http://ip-api.com/json", (res) => {
				var data = [];

				res.on('data', (chunk) => {
					data.push(chunk);
				});

				res.on('end', () => {
					data = Buffer.concat(data);
					data = data.toString();
					data = JSON.parse(data);

					send("PRIVMSG " + chan + " :" + data.query + " [" + data.countryCode + "]");
				});
			}).on('error', (e) => {
				console.log("Got error to get external ip address. (" + e.message + ")");
				send("PRIVMSG " + chan + " :Error.");
			});
		}
	], [
		["!lip"],
		(chan, msg) => {
			var ni = os.networkInterfaces(),
			result = "";

			for (var idx in ni) {
				if (idx.toLowerCase().indexOf("loopback") > -1 ||
					idx.toLowerCase().indexOf("tunneling") > -1 ||
					idx.toLowerCase().indexOf("6to4") > -1 ||
					idx.toLowerCase() == "lo"
				) {
					continue;
				}

				result += idx + ": ";

				if (ni[idx][1]) {
					result += ni[idx][1].address + ", ";
				} else if (ni[idx].length > 0) {
					result += ni[idx][0].address + ", ";
				}

				result = result.substr(0, result.length - 2) + " / ";
			}

			send("PRIVMSG " + chan + " :" + result.substr(0, result.length - 2));
		}
	], [
		["!cpu"],
		(chan, msg) => {
			send("PRIVMSG " + chan + " :" + os.cpus()[0].model);
		}
	], [
		["!메모리", "!mem", "!memory"],
		(chan, msg) => {
			send("PRIVMSG " + chan + " :" + getFreeMemory() + " / " + getTotalMemory());
		}
	]);
}

function keyProc(key) {
	switch (key) {
		case '\u0003':
			send("QUIT :^C");

			setTimeout(() => {
				client.end();
				process.exit(0);
			}, 1000);

			break;

		case 'q':
			send("QUIT :admin pressed quit key.");

			log("exit after 2 seconds...");
			setTimeout(() => {
				client.end();
				process.exit(0);
			}, 2000);

			break;
	}
}

function send(data) {
	if (!client) { return; }

    client.write(data + '\r\n');
}

function log(text) {
	var d = new Date();

	var tzOffset = d.getTimezoneOffset() * 60000;

	var dStr = (new Date(Date.now() - tzOffset)).toISOString()

	console.log(
		"[" + dStr.substring(0, 10) + " " + dStr.substring(11, 19) + "] " + text
	);
}

// -- utils

function getFreeMemory() {
	return memorySizeToStr(os.freemem());
}

function getTotalMemory() {
    return memorySizeToStr(os.totalmem());
}

function memorySizeToStr(mem) {
	if (mem > (1024 * 1024 * 1024)) {
		return (mem / (1024 * 1024 * 1024)).toFixed(2) + " GB";
	} else if (mem > (1024 * 1024)) {
		return (mem / (1024 * 1024)).toFixed(2) + " MB";
	} else if (mem > 1024) {
		return (mem / 1024).toFixed(2) + " KB";
	} else {
		return mem + " bytes";
	}
}

function getUptimeStr() {
	var uptime = new Date(os.uptime() * 1000);
	var result = "";

	if ((uptime.getFullYear() - 1970) > 0) { result += String(uptime.getFullYear() - 1970) + "y "; }
	if (uptime.getMonth() > 0) { result += String(uptime.getMonth()) + "m "; }
	if (uptime.getDate() > 0) { result += String(uptime.getDate()) + "d "; }
	if (uptime.getHours() > 0) { result += String(uptime.getHours()) + "h "; }
	if (uptime.getMinutes() > 0) { result += String(uptime.getMinutes()) + "min "; }
	if (uptime.getSeconds() > 0) { result += String(uptime.getSeconds()) + "s"; }

	return result;
}

// -- main

if (init()) {
	proc();
} else {
	process.exit(1);
}
